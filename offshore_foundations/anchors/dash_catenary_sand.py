#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Bruno Stuyts'

# Django and native Python packages
import warnings

# 3rd party packages
import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import plotly.graph_objs as go
from plotly import tools
from chaincatenary import ChainCatenary

# Project imports

app = dash.Dash()
app.css.append_css({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"})
app.title='Anchor line geometry'

app.layout = html.Div([
        html.Div([
            html.H3("Anchor line geometry - Sand")
        ], className='row text-center'),
        # Geometry input
        html.Div([
            html.Div([
                html.Label('Padeye depth [m]'),
                dcc.Input(
                    id='padeyedepth',
                    placeholder='Enter a value...',
                    type='number',
                    value=6.0
                )], className='col-md-4'),
            html.Div([
                html.Label('Mudline angle [deg]'),
                dcc.Input(
                    id='mudlineangle',
                    placeholder='Enter a value...',
                    type='number',
                    value=4.0
                )], className='col-md-4'),
            html.Div([
                html.Label('Mudline tension [kN]'),
                dcc.Input(
                    id='mudlinetension',
                    placeholder='Enter a value...',
                    type='number',
                    value=1000
                )], className='col-md-4'),
            ], className='row'),
        # Soil properties
        html.Div([
            html.Div([
                html.Label('Effective unit weight [kN/m3]'),
                dcc.Input(
                    id='effectivesoilweight',
                    placeholder='Enter a value...',
                    type='number',
                    value=10.0
                )], className='col-md-4'),
            ], className='row'),
        # Chain properties
        html.Div([
            html.Div([
                html.Label('Chain bouyant weight [kN/m]'),
                dcc.Input(
                    id='chainweight',
                    placeholder='Enter a value...',
                    type='number',
                    value=4.3
                )], className='col-md-4'),
            html.Div([
                html.Label('Nominal chain diameter [m]'),
                dcc.Input(
                    id='chaindiameter',
                    placeholder='Enter a value...',
                    type='number',
                    value=0.16
                )], className='col-md-4'),
            ], className='row'),
        html.Div([dcc.Graph(id='visualization')], style={'width': '90%', 'display': 'inline-block', 'padding': '0 20'}),

])


def chain_geometry(padeyedepth, mudlineangle, mudlinetension, chainweight, chaindiameter, effectivesoilweight):
    """
    Calculates the chain geometry for a given soil strength
    """
    # 1. Pre-calc
    _calc = ChainCatenary(
        padeye_depth=padeyedepth, mudline_chain_angle=mudlineangle, mudline_tension=mudlinetension,
        chain_bouyant_weight=chainweight, chain_diameter=chaindiameter,
        soil_type="SAND", soil_effective_unitweight=effectivesoilweight
    )
    _calc.calculate_catenary()

    return {
        'z [m]': _calc.output['z [m]'],
        'x [m]': _calc.output['x [m]'],
        'Padeye tension [kN]': _calc.calculation_arrays['T'][-1],
        'Padeye angle [deg]': np.rad2deg(np.arctan(_calc.calculation_arrays['tan_theta'][-1]))
    }


@app.callback(
    dash.dependencies.Output('visualization', 'figure'),
    [dash.dependencies.Input('padeyedepth', 'value'),
     dash.dependencies.Input('mudlineangle', 'value'),
     dash.dependencies.Input('mudlinetension', 'value'),
     dash.dependencies.Input('chainweight', 'value'),
     dash.dependencies.Input('chaindiameter', 'value'),
     dash.dependencies.Input('effectivesoilweight', 'value')
     ])
def update_signal(padeyedepth, mudlineangle, mudlinetension, chainweight, chaindiameter, effectivesoilweight):


    result = chain_geometry(
        padeyedepth, mudlineangle, mudlinetension, chainweight, chaindiameter, effectivesoilweight)

    fig = tools.make_subplots(rows=1, cols=1, print_grid=False)
    resistance_trace = go.Scatter(
        x=result['x [m]'],
        y=result['z [m]'],
        showlegend=False)
    fig.append_trace(resistance_trace, 1, 1)
    fig['layout']['xaxis1'].update(title='x [m]', range=[0, 40.0])
    fig['layout']['yaxis1'].update(title='z [m]', autorange='reversed', scaleanchor='x', scaleratio=1.0)
    fig['layout'].update(title='Anchor chain geometry - Angle padeye=%.2fdeg, tension padeye=%.1fkN' % (
        result['Padeye angle [deg]'], result['Padeye tension [kN]']
    ), height=800)
    return fig

if __name__ == '__main__':
    app.run_server(port=8052)