import pandas as pd
import numpy as np
from scipy.optimize import bisect

class ChainCatenary(object):

    def __init__(
            self, padeye_depth, mudline_chain_angle, mudline_tension,
                 chain_bouyant_weight, chain_diameter,
                 soil_type="CLAY", su_mudline=None, su_increase=None,
                 nq_clay_mudline=5.1, nq_clay_deep=7.6, nq_od_deep=6.0,
                 soil_effective_unitweight=None, nq_sand=40.0, friction_factor=0.5, k_user=None, seed=101):

        """
        Initialize input variables

        :param padeye_depth: Depth of the padeye below mudline [m]
        :param mudline_chain_angle: Chain angle at the mudline [deg]
        :param mudline_tension: Chain tension at the mudline [kN]
        :param chain_bouyant_weight: Chain bouyany weight [kN/m]
        :param chain_diameter: Outer diameter of the chain [m]
        :param soil_type: Soil type (choose from "CLAY", "SAND", "CUSTOM" (default = "CLAY")
        :param su_mudline: Undrained shear strenght at the mudline, only used for CLAY [kPa]
        :param su_increase: Undrained shear strength gradient, only used for CLAY [kPa/m]
        :param nq_clay_mudline: Bearing capacity factor for CLAY at the mudline [-] (default=5.1)
        :param nq_clay_deep: Bearing capacity factor for CLAY for deeper depths (full flow-around) [-] (default=7.6)
        :param nq_od_deep: Multiplier on chain diameter which defines where deep behaviour is reached [-] (default=6.0)
        :param soil_effective_unitweight: Effective unit weight of the soil [kN/m3], only used in SAND
        :param nq_sand: Bearing capacity factor Nq in SAND [-] (default=40.0)
        :param friction_factor: Friction factor (ratio F/Q) [-] (default=0.5)
        :param k_user: Ratio of unit bearing over depth [kN/m/m], only used for CUSTOM soil
        :param seed: Number of nodes (default = 101)
        """
        # Initialize and validate input parameters

        if soil_type == "CLAY":
            if su_mudline is None or su_increase is None:
                raise ValueError("CLAY soil: Undrained shear strength at the mudline, undrained shear strength increase"
                                 " need to be specified.")

        elif soil_type == "SAND":
            if soil_effective_unitweight is None or nq_sand is None:
                raise ValueError("SAND soil: Soil submerged unit weight and bearing capacity factor Nq "
                                 "need to be specified.")

        elif soil_type == "CUSTOM":
            if k_user is None:
                raise ValueError("CUSTOM soil: Ratio of unit bearing over depth "
                                 "needs to be specified.")

        else:
            raise ValueError("Soil type not recognized. Use \"CLAY\", \"SAND\" or \"CUSTOM\"")

        # Store the input variables in the ChainCatenary object
        self.padeye_depth = padeye_depth
        self.mudline_chain_angle = mudline_chain_angle
        self.mudline_tension = mudline_tension
        self.chain_bouyant_weight = chain_bouyant_weight
        self.chain_diameter = chain_diameter
        self.soil_type = soil_type
        self.su_mudline = su_mudline
        self.su_increase = su_increase
        self.soil_effective_unitweight = soil_effective_unitweight
        self.nq_sand = nq_sand
        self.friction_factor = friction_factor
        self.nq_clay_mudline = nq_clay_mudline
        self.nq_clay_deep = nq_clay_deep
        self.nq_od_deep = nq_od_deep
        self.k_user = k_user
        self.seed = seed



    def initialize_calc(self):

        self.catenary_control = {'jazz': 1.0,
                                'dQ avg': None,
                                'Q bar': 27.0,
                                'friction': 0.55,
                                'Normalised Tension': None,
                                'Effective normal parameter': 2.5,
                                'Effective tangential parameter': 8.0,
                                'angle mudline': None
                                 }

        self.calculation_output = {'Tension padeye': 0.5 * self.mudline_tension,
                                  'Horizontal distance': self.padeye_depth / np.tan(
                                      self.mudline_chain_angle),
                                  'To over Ta': 2.0,
                                  'Angle padeye': self.mudline_chain_angle,
                                  'Uplift force anchor': 0.5 * self.mudline_tension * np.sin(
                                      np.radians(self.mudline_chain_angle)),
                                   'Horizontal force anchor': 0.5 * self.mudline_tension * np.sin(
                                      np.radians(self.mudline_chain_angle))
                                   }

        self.calculation_arrays = {'Depth': np.zeros(self.seed),
                                   'Shear strength': np.zeros(self.seed),
                                   'Nq': np.zeros(self.seed),
                                   'q': np.zeros(self.seed),
                                   'Q': np.zeros(self.seed),
                                   'F': np.zeros(self.seed),
                                   'mu': np.zeros(self.seed),
                                   'Horizontal distance': np.zeros(self.seed),
                                   'tan_theta': np.ones(self.seed) * np.radians(np.tan(self.mudline_chain_angle)),
                                   'T': np.zeros(self.seed),
                                   }

        # Pre-calc
        if self.soil_type == 'CLAY':
            self.catenary_control['dQ avg'] = self.catenary_control['Effective normal parameter'] * \
                                              self.chain_diameter * 11.0 * self.su_increase
        elif self.soil_type == 'SAND':
            self.catenary_control['dQ avg'] = self.catenary_control['Effective normal parameter'] * \
                                              self.chain_diameter * self.soil_effective_unitweight * \
                                              self.nq_sand
        else:
            self.catenary_control['dQ avg'] = self.k_user

        if self.catenary_control['dQ avg'] == 0.0:
            self.catenary_control['Normalised Tension'] = self.calculation_output['Tension padeye'] / (
            self.padeye_depth * self.catenary_control['Q bar'])
        else:
            self.catenary_control['Normalised Tension'] = 2.0 * self.calculation_output['Tension padeye'] / (
                self.catenary_control['dQ avg'] * self.padeye_depth ** 2.0)

        if self.mudline_chain_angle == 0.0:
            self.catenary_control['angle mudline'] = 1.0e-6
        else:
            self.catenary_control['angle mudline'] = np.radians(self.mudline_chain_angle)

    def calculate_mudline_tension(self, padeyetension):

        self.calculation_output['Tension padeye'] = padeyetension
        self.UpdateCalculationArrays()
        return self.calculation_arrays['T'][0] - self.mudline_tension

    def calculate_catenary(self):
        """
        Actual calculation routine

        Performs an optimization using the bisection algorithm to find the padeye tension which results in the specified
        mudline tension.

        During the optimization routine, the Randoplh & Neubecker (2005) formulae are applied.

        :return: Fills the output attribute of the object with a DataFrame containing details of the calculation
        """

        self.initialize_calc()

        padeyetensionfinal = bisect(self.calculate_mudline_tension, 0.1, self.mudline_tension,
                                    xtol=1e-6)
        self.output = pd.DataFrame({
            'z [m]': self.calculation_arrays['Depth'],
            'Su [kPa]': self.calculation_arrays['Shear strength'],
            'Nq [-]': self.calculation_arrays['Nq'],
            'Q [kN/m]': self.calculation_arrays['Q'],
            'F [kN/m]': self.calculation_arrays['F'],
            'Friction factor [-]': self.calculation_arrays['mu'],
            'x [m]': self.calculation_arrays['Horizontal distance'],
            'theta [deg]': list(np.rad2deg(self.calculation_arrays['tan_theta']))})

    def UpdateCalculationArrays(self):
        # Update arrays for calculations

        for i in range(0, self.seed):

            # Depth, z [m]
            self.calculation_arrays['Depth'][i] = ((i * self.padeye_depth / (self.seed - 1)) * -1) * -1

            # Shear strength [kPa]
            # Nq
            # q [kPa/m]
            if self.soil_type == 'CLAY':
                self.calculation_arrays['Shear strength'][i] = self.su_mudline + self.su_increase * \
                                                                                 self.calculation_arrays[
                                                                                             'Depth'][i]
                if self.calculation_arrays['Depth'][i] == 0.0:
                    self.calculation_arrays['Nq'][i] = self.nq_clay_mudline
                elif self.calculation_arrays['Depth'][i] >= self.nq_od_deep * self.chain_diameter:
                    self.calculation_arrays['Nq'][i] = self.nq_clay_deep
                else:
                    self.calculation_arrays['Nq'][i] = self.nq_clay_mudline + \
                                                       self.calculation_arrays['Depth'][i] * \
                                                       (self.nq_clay_deep - self.nq_clay_mudline) / (
                                                        self.nq_od_deep * self.chain_diameter)
                self.calculation_arrays['q'][i] = self.calculation_arrays['Shear strength'][i] * \
                                                  self.calculation_arrays['Nq'][i]
            elif self.soil_type == 'SAND':
                self.calculation_arrays['Shear strength'][i] = self.soil_effective_unitweight * \
                                                               self.calculation_arrays['Depth'][i]
                self.calculation_arrays['Nq'][i] = None
                self.calculation_arrays['q'][i] = self.calculation_arrays['Shear strength'][i] * self.nq_sand
            else:
                self.calculation_arrays['Shear strength'][i] = None
                self.calculation_arrays['Nq'][i] = None
                self.calculation_arrays['q'][i] = None

            # Horizontal distance, x [m]
            if self.catenary_control['dQ avg'] == 0.0:
                self.calculation_arrays['Horizontal distance'][i] = np.sqrt(
                    2.0 * self.catenary_control['Normalised Tension']) * (np.sqrt(
                    self.catenary_control['Normalised Tension'] * self.catenary_control[
                        'angle mudline'] ** 2.0 / 2.0 + 1.0) - np.sqrt(
                    self.catenary_control['Normalised Tension'] * self.catenary_control['angle mudline'] ** 2.0 / 2.0 +
                    self.calculation_arrays['Depth'][i] / self.padeye_depth)) * self.padeye_depth
            else:
                self.calculation_arrays['Horizontal distance'][i] = np.log((1.0 + np.sqrt(
                    self.catenary_control['Normalised Tension'] * self.catenary_control[
                        'angle mudline'] ** 2.0 / 2.0 + 1.0)) / (self.calculation_arrays['Depth'][i] /
                                                                 self.padeye_depth + np.sqrt(
                    self.catenary_control['Normalised Tension'] * self.catenary_control['angle mudline'] ** 2.0 / 2.0 + (
                        self.calculation_arrays['Depth'][i] / self.padeye_depth) ** 2.0))) / np.sqrt(
                    2.0 / self.catenary_control['Normalised Tension']) * self.padeye_depth

            # dx/dz = tan(theta)
            if i == 0:
                self.calculation_arrays['tan_theta'][i] = self.catenary_control['angle mudline']
            else:
                self.calculation_arrays['tan_theta'][i] = (self.calculation_arrays['Depth'][i] -
                                                           self.calculation_arrays['Depth'][i - 1]) / (
                                                              -self.calculation_arrays['Horizontal distance'][i] +
                                                              self.calculation_arrays['Horizontal distance'][i - 1])

            # Q [kN/m]
            if self.soil_type == 'CUSTOM':
                self.calculation_arrays['Q'][i] = self.k_user * self.calculation_arrays['Depth'][i]
            else:
                self.calculation_arrays['Q'][i] = self.calculation_arrays['q'][i] * self.catenary_control[
                    'Effective normal parameter'] * self.chain_diameter

            # F [kN/m]
            if self.soil_type == 'CLAY':
                self.calculation_arrays['F'][i] = self.calculation_arrays['Shear strength'][i] * self.chain_diameter * \
                                                  self.catenary_control['Effective tangential parameter']
            else:
                if self.calculation_arrays['mu'][i] == 0.0:
                    self.calculation_arrays['F'][i] = 0.5 * self.calculation_arrays['Q'][i]
                else:
                    self.calculation_arrays['F'][i] = self.calculation_arrays['mu'][i] * self.calculation_arrays['Q'][i]
            # Friction mu
            if self.calculation_arrays['Q'][i] != 0.0:
                self.calculation_arrays['mu'][i] = self.calculation_arrays['F'][i] / self.calculation_arrays['Q'][i]
            else:
                self.calculation_arrays['mu'][i] = 0.0

        self.catenary_control['friction'] = np.mean(self.calculation_arrays['mu'])
        self.catenary_control['Q bar'] = np.mean(self.calculation_arrays['Q'])
        if self.catenary_control['dQ avg'] == 0.0:
            self.catenary_control['Normalised Tension'] = self.calculation_output['Tension padeye'] / (
            self.padeye_depth * self.catenary_control['Q bar'])
        else:
            self.catenary_control['Normalised Tension'] = 2.0 * self.calculation_output['Tension padeye'] / (
                self.catenary_control['dQ avg'] * self.padeye_depth ** 2.0)

        for i in range(0, self.seed):
            # 2nd update of Horizontal distance, x [m]
            if self.catenary_control['dQ avg'] == 0.0:
                self.calculation_arrays['Horizontal distance'][i] = np.sqrt(
                    2.0 * self.catenary_control['Normalised Tension']) * (np.sqrt(
                    self.catenary_control['Normalised Tension'] * self.catenary_control[
                        'angle mudline'] ** 2.0 / 2.0 + 1.0) - np.sqrt(
                    self.catenary_control['Normalised Tension'] * self.catenary_control['angle mudline'] ** 2.0 / 2.0 +
                    self.calculation_arrays['Depth'][i] / self.padeye_depth)) * self.padeye_depth
            else:
                self.calculation_arrays['Horizontal distance'][i] = np.log((1.0 + np.sqrt(
                    self.catenary_control['Normalised Tension'] * self.catenary_control[
                        'angle mudline'] ** 2.0 / 2.0 + 1.0)) / (self.calculation_arrays['Depth'][i] /
                                                                 self.padeye_depth + np.sqrt(
                    self.catenary_control['Normalised Tension'] * self.catenary_control['angle mudline'] ** 2.0 / 2.0 + (
                        self.calculation_arrays['Depth'][i] / self.padeye_depth) ** 2.0))) / np.sqrt(
                    2.0 / self.catenary_control['Normalised Tension']) * self.padeye_depth

            # 2nd update of dx/dz = tan(theta)
            if i == 0:
                self.calculation_arrays['tan_theta'][i] = self.catenary_control['angle mudline']
            else:
                self.calculation_arrays['tan_theta'][i] = (self.calculation_arrays['Depth'][i] -
                                                           self.calculation_arrays['Depth'][i - 1]) / (
                                                              -self.calculation_arrays['Horizontal distance'][i] +
                                                              self.calculation_arrays['Horizontal distance'][i - 1])

            # T [kN]
            self.calculation_arrays['T'][i] = self.calculation_output['Tension padeye'] * np.exp(
                self.catenary_control['friction'] * (
                    self.calculation_arrays['tan_theta'][-1] - self.calculation_arrays['tan_theta'][i]))

        # Updating of calculation control variables post-update

        # Updating of output variable post-update
        self.calculation_output['Horizontal distance'] = self.calculation_arrays['Horizontal distance'][0]
        self.calculation_output['To over Ta'] = self.mudline_tension / self.calculation_output[
            'Tension padeye']
        self.calculation_output['Angle padeye'] = np.degrees(np.arctan(self.calculation_arrays['tan_theta'][-1]))
        self.calculation_output['Uplift force anchor'] = 0.5 * self.calculation_output['Tension padeye'] * np.sin(
            np.radians(self.mudline_chain_angle))
        self.calculation_output['Horizontal force anchor'] = 0.5 * self.calculation_output['Tension padeye'] * np.sin(
            np.radians(self.mudline_chain_angle))
