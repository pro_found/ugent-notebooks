#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Bruno Stuyts'

# Django and native Python packages
import warnings

# 3rd party packages
import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import plotly.graph_objs as go
from plotly import tools

# Project imports

app = dash.Dash()
app.css.append_css({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"})
app.title='Suction caisson penetration in clay'

app.layout = html.Div([
        html.Div([
            html.H3("Suction caisson penetration in clay")
        ], className='row text-center'),
        # Geometry input
        html.Div([
            html.Div([
                html.Label('Penetration [m]'),
                dcc.Input(
                    id='penetration',
                    placeholder='Enter a value...',
                    type='number',
                    value=30.0
                )], className='col-md-4'),
            html.Div([
                html.Label('Diameter [m]'),
                dcc.Input(
                    id='diameter',
                    placeholder='Enter a value...',
                    type='number',
                    value=6.0
                )], className='col-md-4'),
            html.Div([
                html.Label('Wall thickness [mm]'),
                dcc.Input(
                    id='wallthickness',
                    placeholder='Enter a value...',
                    type='number',
                    value=45
                )], className='col-md-4'),
            ], className='row'),
        # Soil properties
        html.Div([
            html.Div([
                html.Label('Submerged unit weight [kN/m3]'),
                dcc.Input(
                    id='gammaeff',
                    placeholder='Enter a value...',
                    type='number',
                    value=4.0
                )], className='col-md-4'),
            html.Div([
                html.Label('Undrained shear strength at mudline [kPa]'),
                dcc.Input(
                    id='sumudline',
                    placeholder='Enter a value...',
                    type='number',
                    value=2.0
                )], className='col-md-4'),
            html.Div([
                html.Label('Linear increase of undrained shear strength with depth [kPa/m]'),
                dcc.Input(
                    id='ksu',
                    placeholder='Enter a value...',
                    type='number',
                    value=1.0
                )], className='col-md-4'),
            ], className='row'),
        # Calculation control
        html.Div([
            html.Div([
                html.Label('Linear increase of submerged unit weight [kN/m3/m]'),
                dcc.Input(
                    id='kgamma',
                    placeholder='Enter a value...',
                    type='number',
                    value=0.1
                )], className='col-md-4'),
            html.Div([
                html.Label('Sensitivity [-]'),
                dcc.Input(
                    id='sens',
                    placeholder='Enter a value...',
                    type='number',
                    value=5
                )], className='col-md-4'),
            html.Div([
                html.Label('Vertical load [kN]'),
                dcc.Input(
                    id='verticalload',
                    placeholder='Enter a value...',
                    type='number',
                    value=1000.0
                )], className='col-md-4'),
            ], className='row'),
        html.Div([
            html.Div([
                html.H2(id='requiredunderpressure')
            ], className='col-md-4'),
        ], className='row'),
        html.Div([dcc.Graph(id='visualization')], style={'width': '90%', 'display': 'inline-block', 'padding': '0 20'}),

])


def undrained_suction_penetration(diameter, wallthickness, length, weight,
                                  mudline_su_dss, k_su_dss, sensitivity, gamma_eff_mudline, k_gamma,
                                  sucauc_dss=1.0, sudss_caue=1.0, nc=9.0, lb_ratio=0.67, seed=100):
    """
    Calculates the undrained suction penetration process for suction caisson in soil of linearly increasing
    undrained shear strength
    """
    # 1. Pre-calc
    _alpha = 1.0 / sensitivity
    _internal_diameter = diameter - 2.0 * 0.001 * wallthickness
    _inside_circumf = np.pi * _internal_diameter
    _outside_circumf = np.pi * diameter
    _area_tip = 0.25 * np.pi * ((diameter ** 2.0) - (_internal_diameter ** 2.0))
    _internal_area = 0.25 * np.pi * (_internal_diameter ** 2.0)
    _z = np.linspace(0.0, length, seed)
    # 2. Penetration resistance
    _su_avg_tip_z = ((mudline_su_dss + k_su_dss * _z) / 3.0) * (1.0 + sucauc_dss + (1.0 / sudss_caue))
    _q_side_in_z = _alpha * _inside_circumf * (mudline_su_dss * _z + 0.5 * k_su_dss * (_z ** 2.0))
    _q_side_out_z = _alpha * _outside_circumf * (mudline_su_dss * _z + 0.5 * k_su_dss * (_z ** 2.0))
    _q_tip_z = (nc * _su_avg_tip_z + (gamma_eff_mudline + k_gamma * _z) * _z) * _area_tip
    _q_total_z = _q_side_in_z + _q_side_out_z + _q_tip_z
    # 3. Self-weight penetration
    _index_below = np.argmax([_q_total_z > weight])
    _index_above = _index_below - 1
    try:
        _self_weight_penetration = np.interp(
            weight,
            [_q_total_z[_index_above], _q_total_z[_index_below]],
            [_z[_index_above], _z[_index_below]])
    except:
        _self_weight_penetration = np.NaN
    # 4. Required underpressure
    _required_underpressure = (_q_total_z - weight) / _internal_area
    # 5. Allowable underpressure
    _su_lb_tip_z = _su_avg_tip_z * lb_ratio
    _allowable_underpressure = nc * _su_lb_tip_z + (_q_side_in_z / _internal_area)

    return {
        'Depth [m]': _z,
        'Internal shaft resistance [kN]': _q_side_in_z,
        'External shaft resistance [kN]': _q_side_out_z,
        'Base resistance [kN]': _q_tip_z,
        'Total resistance [kN]': _q_total_z,
        'Self-weight penetration [m]': _self_weight_penetration,
        'Required underpressure [kPa]': _required_underpressure,
        'Allowable underpressure [kPa]': _allowable_underpressure,
        'Underpressure [kN]': _required_underpressure[-1] * _internal_area
    }


@app.callback(
    dash.dependencies.Output('visualization', 'figure'),
    [dash.dependencies.Input('penetration', 'value'),
     dash.dependencies.Input('diameter', 'value'),
     dash.dependencies.Input('wallthickness', 'value'),
     dash.dependencies.Input('gammaeff', 'value'),
     dash.dependencies.Input('sumudline', 'value'),
     dash.dependencies.Input('ksu', 'value'),
     dash.dependencies.Input('kgamma', 'value'),
     dash.dependencies.Input('sens', 'value'),
     dash.dependencies.Input('verticalload', 'value'),
     ])
def update_signal(penetration, diameter, wallthickness, gamma_eff, sumudline, ksu, kgamma, sensitivity, load):


    result = undrained_suction_penetration(
        diameter=diameter,
        wallthickness=wallthickness,
        length=penetration,
        weight=load,
        mudline_su_dss=sumudline,
        k_su_dss=ksu,
        sensitivity=sensitivity,
        gamma_eff_mudline=gamma_eff,
        k_gamma=kgamma)

    result_initial = undrained_suction_penetration(
        diameter=diameter,
        wallthickness=wallthickness,
        length=penetration,
        weight=load,
        mudline_su_dss=sumudline,
        k_su_dss=ksu,
        sensitivity=sensitivity,
        gamma_eff_mudline=gamma_eff,
        k_gamma=kgamma)

    fig = tools.make_subplots(rows=1, cols=2, print_grid=False, shared_yaxes=True)
    underpressure_trace = go.Scatter(
        x=result['Required underpressure [kPa]'],
        y=result['Depth [m]'],
        showlegend=True,
        mode='lines',  # Select from 'markers', 'lines+markers' or 'lines'
        name='Required underpressure')
    fig.append_trace(underpressure_trace, 1, 1)
    underpressure_trace = go.Scatter(
        x=result['Allowable underpressure [kPa]'],
        y=result['Depth [m]'],
        showlegend=True,
        mode='lines',  # Select from 'markers', 'lines+markers' or 'lines'
        name='Allowable underpressure')
    fig.append_trace(underpressure_trace, 1, 1)

    internalskin_trace = go.Scatter(
        x=result['Internal shaft resistance [kN]'],
        y=result['Depth [m]'],
        showlegend=True,
        mode='lines',
        name="Internal shaft resistance")
    fig.append_trace(internalskin_trace, 1, 2)
    externalskin_trace = go.Scatter(
        x=result['External shaft resistance [kN]'],
        y=result['Depth [m]'],
        showlegend=True,
        mode='lines',
        name="External shaft resistance")
    fig.append_trace(externalskin_trace, 1, 2)
    base_trace = go.Scatter(
        x=result['Base resistance [kN]'],
        y=result['Depth [m]'],
        showlegend=True,
        mode='lines',
        name="Base resistance")
    fig.append_trace(base_trace, 1, 2)

    fig['layout']['xaxis2'].update(title='Penetration resistance / force, R [kN]')
    fig['layout']['xaxis1'].update(
        title='Required underpressure [kPa]',
        range=[0.0, 1.1 * max(
            result['Required underpressure [kPa]'].max(), result['Allowable underpressure [kPa]'].max())])
    fig['layout']['yaxis1'].update(title='Depth, z [m]', range=[1.1 * penetration, 0.0])
    fig['layout'].update(title='Penetration resistance and required underpressure', height=800, barmode='stack')
    return fig

@app.callback(
    dash.dependencies.Output(component_id='requiredunderpressure', component_property='children'),
    [dash.dependencies.Input('penetration', 'value'),
     dash.dependencies.Input('diameter', 'value'),
     dash.dependencies.Input('wallthickness', 'value'),
     dash.dependencies.Input('gammaeff', 'value'),
     dash.dependencies.Input('sumudline', 'value'),
     dash.dependencies.Input('ksu', 'value'),
     dash.dependencies.Input('kgamma', 'value'),
     dash.dependencies.Input('sens', 'value'),
     dash.dependencies.Input('verticalload', 'value'),
     ])
def update_signal(penetration, diameter, wallthickness, gamma_eff, sumudline, ksu, kgamma, sensitivity, load):


    result = undrained_suction_penetration(
        diameter=diameter,
        wallthickness=wallthickness,
        length=penetration,
        weight=load,
        mudline_su_dss=sumudline,
        k_su_dss=ksu,
        sensitivity=sensitivity,
        gamma_eff_mudline=gamma_eff,
        k_gamma=kgamma)

    return "Required underpressure = %.2f kPa" % result["Required underpressure [kPa]"][-1]


if __name__ == '__main__':
    app.run_server(port=8053)