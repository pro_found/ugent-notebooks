#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Bruno Stuyts'

"""
Suction caisson penetration in sand

Interactive demo for the calculation of vertical effective stresses, resistance components and
required underpressures during suction penetration in sand. The methods are based on the calculations described
by Byrne & Houlsby (2005).

Running this module requires the Python packages Plotly (https://www.plot.ly) and Dash (https://www.plot.ly/dash/)
to be installated.
 
Start the demo from a command prompt or bash shell with the following command:

python dash_penetrationsand.py

When executed the code starts a local development server connected to an interface in the browser. Dash handles all
complexities of this setup.
"""


# Native Python packages
import warnings
import json

# 3rd party packages
import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import plotly.graph_objs as go
from plotly import tools
from plotly.utils import PlotlyJSONEncoder

# Project imports
from functions import nq_frictionangle_sand, ngamma_frictionangle_davisbooker

#-------------------------------------------------------------------------------------------
# PART 1: App layout

# Building up HTML elements defining the layout of the app
#-------------------------------------------------------------------------------------------

app = dash.Dash()
app.css.append_css({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"})
app.title='Suction caisson penetration in uniform sand'

app.layout = html.Div([
        html.Div([
            html.H3("Suction caisson penetration in sand")
        ], className='row text-center'),
        # Geometry input
        html.Div([
            html.Div([
                html.Label('Penetration [m]'),
                dcc.Input(
                    id='penetration',
                    placeholder='Enter a value...',
                    type='number',
                    value=6.0
                )], className='col-md-4'),
            html.Div([
                html.Label('Diameter [m]'),
                dcc.Input(
                    id='diameter',
                    placeholder='Enter a value...',
                    type='number',
                    value=12.0
                )], className='col-md-4'),
            html.Div([
                html.Label('Wall thickness [mm]'),
                dcc.Input(
                    id='wallthickness',
                    placeholder='Enter a value...',
                    type='number',
                    value=45
                )], className='col-md-4'),
            ], className='row'),
        # Soil properties
        html.Div([
            html.Div([
                html.Label('Submerged unit weight [kN/m3]'),
                dcc.Input(
                    id='gammaeff',
                    placeholder='Enter a value...',
                    type='number',
                    value=8.5
                )], className='col-md-4'),
            html.Div([
                html.Label('Friction angle [deg]'),
                dcc.Input(
                    id='frictionangle',
                    placeholder='Enter a value...',
                    type='number',
                    value=44.0
                )], className='col-md-4'),
            html.Div([
                html.Label('Coefficient of lateral earth pressure [-]'),
                dcc.Input(
                    id='lateralcoeff',
                    placeholder='Enter a value...',
                    type='number',
                    value=1.0
                )], className='col-md-4'),
            ], className='row'),
        # Calculation control
        html.Div([
            html.Div([
                html.Label('Suction [kPa]'),
                dcc.Input(
                    id='suction',
                    placeholder='Enter a value...',
                    type='number',
                    value=0,
                    step=10,
                )], className='col-md-4'),
            html.Div([
                html.Label('Ratio internal/external permeability [-]'),
                dcc.Input(
                    id='kf',
                    placeholder='Enter a value...',
                    type='number',
                    value=2
                )], className='col-md-4'),
            html.Div([
                html.Label('Vertical load [kN]'),
                dcc.Input(
                    id='verticalload',
                    placeholder='Enter a value...',
                    type='number',
                    value=6622.0
                )], className='col-md-4'),
            ], className='row'),
        # Chart with vertical effective stresses
        html.Div([dcc.Graph(id='visualization')], style={'width': '90%', 'display': 'inline-block', 'padding': '0 20'}, className='row'),
        # Table with calculation results
        html.Div([dcc.Graph(id='table')], style={'width': '90%', 'display': 'inline-block', 'padding': '0 20'}, className='row'),
        # Hidden div for detailed output
        html.Div(id='intermediate-value', style={'display': 'none'})
])

#-------------------------------------------------------------------------------------------
# PART 2: Calculations

# Solve the differential equation for internal and external vertical effective stresses
# numerically and calculate the resistance components.
#-------------------------------------------------------------------------------------------

def caisson_penetration_resistance_uniform(
        penetration, diameter_outside, diameter_inside,
        gamma_eff, lateral_earthpressure_coefficient, delta, friction_angle, roughness_factor=0.67,
        suction=0.0, fo=1.5, kf=1.0, c0=0.45, c1=0.36, c2=0.48, seed=250):
    """
    Calculates the resistance to penetration for a suction caisson in uniform sand
    according to the method defined by Houlsby and Byrne (2005).
    """
    _wall_thickness = 0.5 * (diameter_outside - diameter_inside)
    _z = np.linspace(0.0, penetration, seed)

    # Coefficient a
    _a1 = c0 - c1 * (1.0 - np.exp(-(penetration / (c2 * diameter_outside))))
    _a = (_a1 * kf) / ((1.0 - _a1) + _a1)

    # Internal vertical effective stress
    _zi = diameter_inside / (4.0 * lateral_earthpressure_coefficient * np.tan(np.radians(delta)))
    _sigma_v_inside = np.array([0.0, ])
    for i, z_i in enumerate(_z):
        if i != 0:
            _gamma_mod = gamma_eff - ((1.0 - _a) * suction / penetration)
            _dz = z_i - _z[i - 1]
            _sigma_v_next = (_gamma_mod * _dz + _sigma_v_inside[i - 1] * (1.0 + (0.5 * _dz / _zi))) / \
                            (1.0 - (0.5 * _dz / _zi))
            _sigma_v_inside = np.append(_sigma_v_inside, _sigma_v_next)
    if np.any(_sigma_v_inside < 0.0):
        _piping = True
    else:
        _piping = False

    # External vertical effective stress
    _sigma_v_outside = np.array([0.0, ])
    for i, z_i in enumerate(_z):
        if i != 0:
            _gamma_mod = gamma_eff + (_a * suction / penetration)
            _dz = z_i - _z[i - 1]
            _zo_mod = (2.0 * _dz * lateral_earthpressure_coefficient * np.tan(np.radians(delta))) / \
                      (diameter_outside * (((1.0 + ((fo * (z_i + _z[i - 1])) / (diameter_outside))) ** 2.0) - 1.0))
            _sigma_v_next = (_gamma_mod * _dz + (1.0 + _zo_mod) * _sigma_v_outside[i - 1]) / (1.0 - _zo_mod)
            _sigma_v_outside = np.append(_sigma_v_outside, _sigma_v_next)

    # Cumulative friction
    _friction_outside = np.array([0.0, ])
    _friction_inside = np.array([0.0, ])
    for i, z_i in enumerate(_z):
        if i != 0:
            _dz = z_i - _z[i - 1]
            _df_outside = _sigma_v_outside[i] * _dz * lateral_earthpressure_coefficient * \
                          np.tan(np.radians(delta)) * (np.pi * diameter_outside)
            _df_inside = _sigma_v_inside[i] * _dz * lateral_earthpressure_coefficient * \
                         np.tan(np.radians(delta)) * (np.pi * diameter_inside)
            _friction_outside = np.append(_friction_outside, _df_outside)
            _friction_inside = np.append(_friction_inside, _df_inside)
    _internal_shaft_friction = np.cumsum(_friction_inside)[-1]
    _external_shaft_friction = np.cumsum(_friction_outside)[-1]

    # End bearing component
    _nq = nq_frictionangle_sand(friction_angle)['Nq [-]']
    _ngamma = ngamma_frictionangle_davisbooker(friction_angle, roughness_factor=roughness_factor)['Ngamma [-]']
    _end_bearing = (_sigma_v_inside[-1] * _nq + gamma_eff * _wall_thickness * _ngamma) * \
                   (np.pi * diameter_outside * _wall_thickness)

    # Total resistance
    _pressure_reduction = 0.25 * suction * np.pi * (diameter_inside ** 2.0)
    _total_resistance = _internal_shaft_friction + _external_shaft_friction + _end_bearing

    return {
        'Depth [m]': _z,
        'Piping': _piping,
        'Internal vertical effective stress [kPa]': _sigma_v_inside,
        'External vertical effective stress [kPa]': _sigma_v_outside,
        'Shaft resistance internal [kN]': _internal_shaft_friction,
        'Shaft resistance external [kN]': _external_shaft_friction,
        'Annular end bearing [kN]': _end_bearing,
        'Pressure reduction [kN]': _pressure_reduction,
        'Total resistance [kN]': _total_resistance,
        'Coefficient a': _a
    }

#-------------------------------------------------------------------------------------------
# PART 3: Chart and table updating

# Dynamically updates charts and tables based on the input specified by the user.
# The functions decorated with @app.callback are dynamically updated by Dash
#-------------------------------------------------------------------------------------------

@app.callback(
    dash.dependencies.Output('intermediate-value', 'children'),
    [dash.dependencies.Input('penetration', 'value'),
     dash.dependencies.Input('diameter', 'value'),
     dash.dependencies.Input('wallthickness', 'value'),
     dash.dependencies.Input('gammaeff', 'value'),
     dash.dependencies.Input('frictionangle', 'value'),
     dash.dependencies.Input('lateralcoeff', 'value'),
     dash.dependencies.Input('suction', 'value'),
     dash.dependencies.Input('kf', 'value'),
     dash.dependencies.Input('verticalload', 'value'),
     ])
def update_output(penetration, diameter, wallthickness, gamma_eff, frictionangle, lateralcoeff, suction, kf, load):
    """
    Creates JSON formatted output containing the result for underpressure penetration. The result for zero
    suction pressure is also calculated to compare resistance components
    :param penetration: Skirt penetration below mudline [m]
    :param diameter: Outer diameter of the suction caisson [m]
    :param wallthickness: Wall thickness of the skirts [mm]
    :param gamma_eff: Effective unit weight of soil [kN/m3]
    :param frictionangle: Drained friction angle of the soil [deg]
    :param lateralcoeff: Coefficient of lateral earth pressure at rest [-]
    :param suction: Applied suction pressure [kPa]
    :param kf: Ratio of internal to external permeability [-]
    :param load: Self-weight on the caisson [kN]
    :return:
    """
    inside_diameter = diameter - 0.002 * wallthickness
    # Calculate resistance components with underpressure applied
    result = caisson_penetration_resistance_uniform(
        penetration=penetration, diameter_outside=diameter, diameter_inside=inside_diameter,
        gamma_eff=gamma_eff, lateral_earthpressure_coefficient=lateralcoeff, delta=frictionangle-5.0,
        friction_angle=frictionangle, suction=suction, kf=kf)
    # Calculate resistance components without underpressure applied
    result_nopressure = caisson_penetration_resistance_uniform(
        penetration=penetration, diameter_outside=diameter, diameter_inside=inside_diameter,
        gamma_eff=gamma_eff, lateral_earthpressure_coefficient=lateralcoeff, delta=frictionangle - 5.0,
        friction_angle=frictionangle, suction=0.0, kf=kf)
    # Create chart for vertical effective stresses and resistance components
    # Panel 1: Scatter plot for vertical effective stresses
    # Panel 2: Bar chart for resistance components
    fig = tools.make_subplots(rows=1, cols=2, print_grid=False)
    # Trace for internal vertical effective stress
    _data = go.Scatter(
        x=result['Internal vertical effective stress [kPa]'],
        y=result['Depth [m]'],
        showlegend=True,
        mode='lines',
        name='Internal vertical effective stress',
        line=dict(color='green'))
    fig.append_trace(_data, 1, 1)
    # Trace for external vertical effective stress
    _data = go.Scatter(
        x=result['External vertical effective stress [kPa]'],
        y=result['Depth [m]'],
        showlegend=True,
        mode='lines',  # Select from 'markers', 'lines+markers' or 'lines'
        name='External vertical effective stress',
        line=dict(color='red'))
    fig.append_trace(_data, 1, 1)
    # Bar for external load
    _data = go.Bar(
        x=['Vertical load', 'Vertical resistance'],
        y=[load, 0.0],
        name="External load",
        marker=dict(color='orange'))
    fig.append_trace(_data, 1, 2)
    # Bar for pressure-induced load
    _data = go.Bar(
        x=['Vertical load', 'Vertical resistance'],
        y=[result['Pressure reduction [kN]'], 0.0],
        name="Underpressure",
        marker=dict(color='blue'))
    fig.append_trace(_data, 1, 2)
    # Bar for internal shaft resistance
    _data = go.Bar(
        x=['Vertical load', 'Vertical resistance'],
        y=[0.0, result['Shaft resistance internal [kN]']],
        name="Internal shaft",
        marker=dict(color='green'))
    fig.append_trace(_data, 1, 2)
    # Bar for external shaft resistance
    _data = go.Bar(
        x=['Vertical load', 'Vertical resistance'],
        y=[0.0, result['Shaft resistance external [kN]']],
        name="External shaft",
        marker=dict(color='red'))
    fig.append_trace(_data, 1, 2)
    # Bar for base resistance
    _data = go.Bar(
        x=['Vertical load', 'Vertical resistance'],
        y=[0.0, result['Annular end bearing [kN]']],
        name="Base",
        marker=dict(color='brown'))
    fig.append_trace(_data, 1, 2)
    # Update axes
    fig['layout']['xaxis1'].update(
        title='Vertical effective stress [kPa]', side='top', anchor='y',
        range=(result['Internal vertical effective stress [kPa]'].min(),
               1.5 * result_nopressure['Internal vertical effective stress [kPa]'].max()))
    fig['layout']['yaxis1'].update(title='Depth [m]', range=[result['Depth [m]'].max(), 0.0])
    fig['layout']['yaxis2'].update(
        title='Load or resistance [kN]', range=(0.0, 1.1 * max(
            result['Pressure reduction [kN]'] + load,
            result['Shaft resistance external [kN]'] + result['Shaft resistance internal [kN]'] +
            result['Annular end bearing [kN]']
        )))
    # Update figure properties
    fig['layout'].update(height=500, width=1400,
                         hovermode='closest', barmode='stack')
    # Write figure data to JSON
    redata = json.loads(json.dumps(fig.data, cls=PlotlyJSONEncoder))
    relayout = json.loads(json.dumps(fig.layout, cls=PlotlyJSONEncoder))
    cleaned_result = { your_key: result[your_key] for your_key in ['Shaft resistance internal [kN]',
        'Shaft resistance external [kN]',
        'Annular end bearing [kN]',
        'Pressure reduction [kN]',
        'Total resistance [kN]'] }
    cleaned_result_nopressure = { your_key: result_nopressure[your_key] for your_key in ['Shaft resistance internal [kN]',
        'Shaft resistance external [kN]',
        'Annular end bearing [kN]',
        'Pressure reduction [kN]',
        'Total resistance [kN]'] }
    # Return JSON data for use by other functions
    return json.dumps(
        {'data': redata,
         'layout': relayout,
         'result': cleaned_result,
         'result_nopressure': cleaned_result_nopressure,
         'weight': load})

@app.callback(dash.dependencies.Output('visualization', 'figure'),
              [dash.dependencies.Input('intermediate-value', 'children')])
def update_graph(jsonified_cleaned_data):
    """
    Update the display of the figure with vertical effective stresses and resistance components
    :param jsonified_cleaned_data: JSON data from previous function
    :return: Updated figure
    """
    v = json.loads(jsonified_cleaned_data)
    fig = go.Figure(data=v['data'], layout=v['layout'])
    return fig

@app.callback(dash.dependencies.Output('table', 'figure'),
              [dash.dependencies.Input('intermediate-value', 'children')])
def update_table(jsonified_cleaned_data):
    """
    Update the display of the table with resistance components
    :param jsonified_cleaned_data: JSON data from previous function
    :return: Updated table
    """
    # Load JSON data
    v = json.loads(jsonified_cleaned_data)
    # Create a table with numerical values of resistance components
    trace = go.Table(
        header=dict(values=['Component', 'Value [kN]', 'Value with no underpressure [kN]'],
                    line=dict(color='#7D7F80'),
                    fill=dict(color='#a1c3d1'),
                    align=['left'] * 5),
        cells=dict(values=[['Internal shaft resistance', 'External shaft resistance', 'Base resistance',
                            '<b>Total resistance</b>', 'Pressure reduction', 'Self-weight load', '<b>Total load</b>'],
                           [round(v['result']['Shaft resistance internal [kN]'],0),
                            round(v['result']['Shaft resistance external [kN]'],0),
                            round(v['result']['Annular end bearing [kN]'],0),
                             '<b>%.0f</b>' % v['result']['Total resistance [kN]'],
                            round(v['result']['Pressure reduction [kN]'],0),
                            round(v['weight'],0),
                            '<b>%.0f</b>' % (v['result']['Pressure reduction [kN]'] + v['weight'])],
                            [round(v['result_nopressure']['Shaft resistance internal [kN]'],0),
                            round(v['result_nopressure']['Shaft resistance external [kN]'],0),
                            round(v['result_nopressure']['Annular end bearing [kN]'],0),
                             '<b>%.0f</b>' % v['result_nopressure']['Total resistance [kN]'],
                            round(v['result_nopressure']['Pressure reduction [kN]'],0),
                            round(v['weight'],0),
                             '<b>%.0f</b>' % (v['result_nopressure']['Pressure reduction [kN]'] + v['weight'])],
                            ],
                   line=dict(color='#7D7F80'),
                   fill=dict(color='#EDFAFF'),
                   align=['left'] * 5))
    layout = dict(width=800, height=400)
    data = [trace]
    fig = dict(data=data, layout=layout)
    return fig

if __name__ == '__main__':
    app.run_server(port=8051)