#!/usr/bin/env python
# -*- coding: utf-8 -*-
import base64

__author__ = 'Bruno Stuyts'

# Django and native Python packages

# 3rd party packages

# Project imports
import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import pandas as pd
import plotly.graph_objs as go
from plotly import tools

app = dash.Dash()
app.title='Cyclic DSS test'

cyclic = pd.read_excel("Data/CDSS_data.xlsx", sheet_name="Cyclic")
static = pd.read_excel("Data/CDSS_data.xlsx", sheet_name="Static")
cycle = 0

# New cycle starts when shear stress becomes positive after having been negative
for i, row in cyclic.iterrows():
    if i > 0:
        if row['Shear Stress (kPa)'] >= 0.0 and cyclic.loc[i-1, 'Shear Stress (kPa)'] < 0.0:
            cycle+=1
        cyclic.loc[i, "Cycle"] = cycle

cyclic['tau [kPa]'] = 176.3 * cyclic['tau/sigma_eff']
cyclic['sigma [kPa]'] = 176.3 * cyclic['sigma_vo_eff/sigma_vo_eff_0']
cyclic['u [kPa]'] = 176.3 - cyclic['sigma [kPa]']

cyclic_summary = pd.DataFrame()
for i, cyc in enumerate(cyclic['Cycle'].unique()):
    cyclic_summary.loc[i, "Cycle"] = cyc
    cyclic_summary.loc[i, "Shear strain (pct)"] = cyclic[cyclic['Cycle']==cyc]['Shear Strain (pct)'].max()
    cyclic_summary.loc[i, "u (kPa)"] = cyclic[cyclic['Cycle']==cyc]['u [kPa]'].max()

fig = tools.make_subplots(rows=2, cols=2)

_data = go.Scatter(
    x=cyclic['Shear Strain (pct)'],
    y=cyclic['Shear Stress (kPa)'],
    showlegend=False,
    marker = dict(
        size = 10,
        color = 'rgba(189, 189, 189, .8)',
        ))
fig.append_trace(_data, 1, 1)

_data = go.Scatter(
    x=static[static['Shear Stress (kPa)'] < 100.0]['Shear Strain (pct)'],
    y=static[static['Shear Stress (kPa)'] < 100.0]['Shear Stress (kPa)'],
    showlegend=False,
    name='Static',
    marker = dict(
        size = 10,
        color = 'rgba(0, 165, 0, .8)',
        ),
)
#fig.append_trace(_data, 1, 1)

_data = go.Scatter(
    x=cyclic['sigma [kPa]'] - 5.0,
    y=cyclic['tau [kPa]'],
    showlegend=False,
    marker = dict(
        size = 10,
        color = 'rgba(189, 189, 189, .8)',
        ),
    )
fig.append_trace(_data, 1, 2)

_data = go.Scatter(
    x=static[static['Shear Stress (kPa)'] < 100.0]['Vertical Stress (kPa)'],
    y=static[static['Shear Stress (kPa)'] < 100.0]['Shear Stress (kPa)'],
    name="Static",
    marker = dict(
        size = 10,
        color = 'rgba(0, 165, 0, .8)',
        ),
)
#fig.append_trace(_data, 1, 2)

_data = go.Scatter(
    x=[0.0, 261.4],
    y=[0.0, 150.0],
    showlegend=False,
    name="static-envelope",
    mode='lines',
    marker = dict(
        size = 10,
        color = 'rgba(0, 165, 0, .8)',
        ),
    line=dict(
        color=('rgb(0, 165, 0)'),
        width=4,
        dash='dash')
)
fig.append_trace(_data, 1, 2)

_data = go.Scatter(
    x=cyclic_summary['Cycle'],
    y=cyclic_summary['Shear strain (pct)'],
    showlegend=False,
    mode='lines+markers',
    marker = dict(
        size = 10,
        color = 'rgba(189, 189, 189, .8)',
        ))
fig.append_trace(_data, 2, 1)

_data = go.Scatter(
    x=cyclic_summary['Cycle'],
    y=cyclic_summary['u (kPa)'] / 172.0,
    showlegend=False,
    mode='lines+markers',
    marker = dict(
        size = 10,
        color = 'rgba(189, 189, 189, .8)',
        ))
fig.append_trace(_data, 2, 2)

fig['layout']['xaxis1'].update(title='Shear strain (%)')
fig['layout']['yaxis1'].update(title='Shear stress (kPa)')
fig['layout']['xaxis2'].update(title='Effective normal stress (kPa)', range=[0.0, 200.0])
fig['layout']['yaxis2'].update(title='Shear stress (kPa)')
fig['layout']['xaxis3'].update(title='Cycle number, N')
fig['layout']['yaxis3'].update(title='Shear strain (%)')
fig['layout']['xaxis4'].update(title='Cycle number, N')
fig['layout']['yaxis4'].update(title='Excess pore pressure (kPa)')

fig['layout'].update(height=800,
    hovermode= 'closest',
    title='Example cyclic DSS test')

app.layout = html.Div([

    html.Div([

        html.Div([
            html.Label('Cycle number'),
            dcc.Slider(
                id='n--slider',
                min=0.0,
                max=42,
                value=1,
                marks={str(mark): str(round(mark,0)) for mark in (1e-8 +  np.linspace(1, 42, 42))}
            ),

        ],
        style={'width': '90%', 'display': 'inline-block', 'padding': '0px 20px 20px 20px'}),

        html.Div([
            # html.Label('Average load [kN]'),
            # dcc.Slider(
            #     id='average--slider',
            #     min=-8000.0,
            #     max=10000.0,
            #     value=2000.0,
            #     marks={str(year): str(round(year, 0)) for year in (1e-8 + np.linspace(-8000.0, 10000.0, 11))}
            # ),
        ], style={'width': '30%', 'display': 'inline-block', 'float': 'right', 'padding': '0px 20px 20px 20px'})
    ], style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 5px'
    }),

    html.Div([
        dcc.Graph(id='cdss-visualization')
    ], style={'width': '100%', 'display': 'inline-block', 'padding': '0 20'}),
    html.Script([
        'var waitForPlotly = setInterval( function() {'
        'if( typeof(window.Plotly) !== "undefined" ){'
        'MathJax.Hub.Config({ SVG: { font: "STIX-Web" }, displayAlign: "center" });'
        'MathJax.Hub.Queue(["setRenderer", MathJax.Hub, "SVG"]);'
        'clearInterval(waitForPlotly);'
        '}}, 250 );'
    ]),
])

@app.callback(
    dash.dependencies.Output('cdss-visualization', 'figure'),
    [dash.dependencies.Input('n--slider', 'value'),])
def update_figure(cycle):
    for _data in fig.data:
        if _data['name'] in ['Static', 'static-envelope']:
            pass
        else:
            _data.update(marker=dict(
                size=10,
                color='rgba(189, 189, 189, .8)',
            ))

    CYCLE = int(cycle)

    _data = go.Scatter(
        x=cyclic[cyclic['Cycle'] == CYCLE]['Shear Strain (pct)'],
        y=cyclic[cyclic['Cycle'] == CYCLE]['Shear Stress (kPa)'],
        showlegend=False,
        marker=dict(
            size=10,
            color='rgba(255, 0, 0, .8)',
        ))
    fig.append_trace(_data, 1, 1)

    _data = go.Scatter(
        x=cyclic[cyclic['Cycle'] == CYCLE]['sigma [kPa]'] - 5.0,
        y=cyclic[cyclic['Cycle'] == CYCLE]['tau [kPa]'],
        showlegend=False,
        marker=dict(
            size=10,
            color='rgba(255, 0, 0, .8)',
        ))
    fig.append_trace(_data, 1, 2)

    _data = go.Scatter(
        x=cyclic_summary[cyclic_summary['Cycle'] == CYCLE]['Cycle'],
        y=cyclic_summary[cyclic_summary['Cycle'] == CYCLE]['Shear strain (pct)'],
        showlegend=False,
        mode='markers',
        marker=dict(
            size=10,
            color='rgba(255, 0, 0, .8)',
        ))
    fig.append_trace(_data, 2, 1)

    _data = go.Scatter(
        x=cyclic_summary[cyclic_summary['Cycle'] == CYCLE]['Cycle'],
        y=cyclic_summary[cyclic_summary['Cycle'] == CYCLE]['u (kPa)'] / 172.0,
        showlegend=False,
        mode='markers',
        marker=dict(
            size=10,
            color='rgba(255, 0, 0, .8)',
        ))
    fig.append_trace(_data, 2, 2)

    return fig

if __name__ == '__main__':
    app.run_server()