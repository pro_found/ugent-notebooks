import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import plotly.graph_objs as go

app = dash.Dash()
app.title='Pipeline laydown shape'

MAX_DEPTH = 200.0
MAX_TENSION = 200.0
MAX_WEIGHT = 10.0

app.layout = html.Div([
    html.Div([

        html.Div([
            html.Label('Water depth [m]'),
            dcc.Slider(
                id='depth--slider',
                min=0.0,
                max=MAX_DEPTH,
                value=0.3 * MAX_DEPTH,
                marks={str(year): str(round(year,0)) for year in (1e-8 +  np.linspace(0.0, MAX_DEPTH, 11))}
            ),

        ],
        style={'width': '30%', 'display': 'inline-block', 'padding': '0px 20px 20px 20px'}),

        html.Div([
            html.Label('Bottom tension [kN]'),
            dcc.Slider(
                id='tension--slider',
                min=0.0,
                max=MAX_TENSION,
                value=0.3 * MAX_TENSION,
                marks={str(year): str(round(year, 0)) for year in (1e-8 + np.linspace(0.0, MAX_TENSION, 11))}
            ),
        ], style={'width': '30%', 'display': 'inline-block', 'float': 'right', 'padding': '0px 20px 20px 20px'}),

        html.Div([
            html.Label('Pipe weight [kN/m]'),
            dcc.Slider(
                id='weight--slider',
                min=0.0,
                max=MAX_WEIGHT,
                value=0.3 * MAX_WEIGHT,
                marks={str(year): str(round(year, 0)) for year in (1e-8 + np.linspace(0.0, MAX_WEIGHT, 11))}
            ),
        ], style={'width': '30%', 'display': 'inline-block', 'float': 'right', 'padding': '0px 20px 20px 20px'}),


    ], style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 5px'
    }),

    html.Div([
        dcc.Graph(id='pipe-visualization')
    ], style={'width': '90%', 'display': 'inline-block', 'padding': '0 20'}),

])


def pipeshape(water_depth, bottom_tension, pipe_weight):
    """
    Calculates the pipeline shape according to the catenary calculation

    :param water_depth: Offset between touchdown point and vessel departure point [m]
    :param bottom_tension: Tension applied at the bottom [kN]
    :param lay_angle: Lay angle applied at the vessel [deg]
    :param pipe_weight: Submerged weight of the pipe per unit line length [kN/m]
    """
    param_a = bottom_tension / pipe_weight

    c_2 = -param_a
    c_1 = 0.0

    y = np.linspace(0.0, water_depth, 5000)
    x = param_a * (np.arccosh((y - c_2) / param_a) - c_1)

    return {
        'x [m]': x,
        'y [m]': y,
        'lay angle [deg]': np.degrees(np.arctan((y[-2] - y[-1]) / (x[-2] - x[-1]))),
        'x vessel [m]': x[-1]
    }

@app.callback(
    dash.dependencies.Output('pipe-visualization', 'figure'),
    [dash.dependencies.Input('depth--slider', 'value'),
     dash.dependencies.Input('tension--slider', 'value'),
     dash.dependencies.Input('weight--slider', 'value'),
     ])
def update_signal(depth, tension, weight):

    shape = pipeshape(water_depth=depth, bottom_tension=tension, pipe_weight=weight)

    return {
        'data': [
            go.Scatter(
                x=shape['x [m]'],
                y=shape['y [m]'],
                showlegend=True,
                mode='lines',  # Select from 'markers', 'lines+markers' or 'lines'
                name='Water depth = %.0fm, Bottom tension=%.0fkN, Pipe weight=%.1fkN/m, Laydown angle=%.1f°' % (
                    depth, tension, weight, shape['lay angle [deg]'])),
        ],
        'layout': go.Layout(
            xaxis={
                'title': "x [m]",
                'type': 'linear'
            },
            yaxis={
                'title': "y [m]",
                'type': 'linear',
                'scaleanchor': 'x',
                'scaleratio': 1.0
            },
            height=600,
            hovermode='closest'
        )
    }

if __name__ == '__main__':
    app.run_server(port=8051)